//
//  CategoriesSectionController.swift
//  RuangGuruTrivia
//
//  Created by Solihin Chiko on 1/3/18.
//  Copyright © 2018 test. All rights reserved.
//

import UIKit
import IGListKit

final class CategoriesItem: NSObject {
    let categoryID: Int
    let name: String
    let imageURL: String
    
    init(categoryID: Int, name: String, imageURL: String) {
        self.categoryID = categoryID
        self.name = name
        self.imageURL = imageURL
    }
}

extension CategoriesItem: ListDiffable {
    func diffIdentifier() -> NSObjectProtocol {
        return categoryID as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        if self === object { return true }
        return false
    }
}

final class CategoriesSectionController: ListSectionController {
    private var object: TriviaCategories?
    
    override init() {
        super.init()
        inset = UIEdgeInsets(top: 10, left: 5, bottom: 0, right: 5)
        self.minimumInteritemSpacing = 0
        self.minimumLineSpacing = 5
    }
    
    override func sizeForItem(at index: Int) -> CGSize {
        return CGSize(width: collectionContext!.containerSize.width/2 - 10, height: (collectionContext!.containerSize.width/2 - 30) * 0.9)
    }
    
    override func cellForItem(at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(withNibName: CategoryCollectionViewCell.nibName, bundle: Bundle.main, for: self, at: index)
        if let cell = cell as? CategoryCollectionViewCell {
            cell.text = object?.name
            cell.imageURL = object?.imageURL
            cell.imageIconURL = object?.imageIconURL
        }
        return cell
    }
    
    override func didUpdate(to object: Any) {
        self.object = object as? TriviaCategories
    }
    
    override func didSelectItem(at index: Int) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller:GamesViewController = storyboard.instantiateViewController(withIdentifier: "GamesViewController") as! GamesViewController
        controller.title = object?.name
        controller.ID = (object?.categoryID)!
        viewController?.navigationController?.pushViewController(controller, animated: true)
    }
    
}
