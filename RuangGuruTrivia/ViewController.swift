//
//  ViewController.swift
//  RuangGuruTrivia
//
//  Created by Solihin Chiko on 1/3/18.
//  Copyright © 2018 test. All rights reserved.
//

import UIKit
import IGListKit
import MBProgressHUD
import RealmSwift

class ViewController: UIViewController, ListAdapterDataSource, ListSingleSectionControllerDelegate {

    @IBOutlet weak var collectionView: UICollectionView!
    
    lazy var adapter: ListAdapter = {
        return ListAdapter(updater: ListAdapterUpdater(), viewController: self)
    }()
    
    var categories:[TriviaCategories] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Select Category"
        adapter.collectionView = collectionView
        adapter.dataSource = self
        
        navigationController?.navigationBar.barTintColor = Colors.vividPurple()
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]

        view.backgroundColor = Colors.colorBG()
        collectionView.backgroundColor = Colors.colorBG()
        collectionView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 20, right: 0)
        
        let trivia:Results<TriviaCategories> = uiRealm.objects(TriviaCategories.self)
        guard trivia.count > 0 else {
            if API.isConnectedToInternet() {
                fetchCategories()
            } else {
                let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
                hud.mode = .text
                hud.label.text = "Not connect to internet"
                hud.hide(animated: true, afterDelay: 2)
            }
            return
        }
        
        categories = []
        for cat in trivia {
            categories.append(cat)
        }
        self.adapter.performUpdates(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: - Function
    func fetchCategories() {
        
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.label.text = "Loading"
        hud.isUserInteractionEnabled = false
        
        API.getTriviaCategories(completion: { (Results) in
            hud.hide(animated: true)
            
            let status:[String:Any] = Results.first!
            let results:[String:Any] = Results.last!
            
            if status["status"] as! String == "Failed" {
                let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
                hud.mode = .text
                hud.label.text = results["response"] as? String
                hud.hide(animated: true, afterDelay: 2)
            }
            else {
                for result in results["response"] as! [[String:Any]] {
                    
                    let name = result["name"] as! String
                    if name == "General Knowledge" ||
                        name == "Entertainment: Books" ||
                        name == "Entertainment: Film" ||
                        name == "Entertainment: Music" ||
                        name == "Entertainment: Video Games" ||
                        name == "Entertainment: Television" ||
                        name == "Science: Computers" ||
                        name == "Celebrities" ||
                        name == "History" ||
                        name == "Animals" {
                        
                        let category = TriviaCategories()
                        category.categoryID = result["id"] as! Int
                        category.name = result["name"] as! String
                        
                        var URL = ""
                        var ICON = ""
                        if name == "General Knowledge" {
                            URL = "https://upload.wikimedia.org/wikipedia/commons/6/65/Little_Green_Street_London.jpg"
                            ICON = "icons8-globe-earth"
                        } else if name == "Entertainment: Books" {
                            URL = "https://arena.westsussex.gov.uk/documents/20889/0/108584903+Renew+items.jpg/ae1f638f-edd5-4c78-b6e3-9612b11bc7fd?t=1498568714460"
                            ICON = "icons8-book-shelf"
                        } else if name == "Entertainment: Film" {
                            URL = "https://media-cdn.tripadvisor.com/media/photo-s/0b/78/38/d6/comfortable-and-accessible.jpg"
                            ICON = "icons8-film-reel"
                        } else if name == "Entertainment: Music" {
                            URL = "https://cdn.londonandpartners.com/visit/london-organisations/alexandra-palace/92923-640x360-alexandra-palace-gig-640.jpg"
                            ICON = "icons8-guitar"
                        } else if name == "Entertainment: Video Games" {
                            URL = "https://i2-prod.mirror.co.uk/incoming/article3986406.ece/ALTERNATES/s615/Kids-playing-a-video-game.jpg"
                            ICON = "icons8-game-controller"
                        } else if name == "Entertainment: Television" {
                            URL = "https://pbs.twimg.com/media/CqPVmGuXEAQIPNi.jpg"
                            ICON = "icons8-retro-tv"
                        } else if name == "Science: Computers" {
                            URL = "https://www.smashinglists.com/wp-content/uploads/2010/07/Mac-128k-600x399.jpg"
                            ICON = "icons8-laptop"
                        } else if name == "Celebrities" {
                            URL = "https://drraa3ej68s2c.cloudfront.net/wp-content/uploads/2017/02/26223655/37bc171bbec873547ca19161aaf1b1d43acf66d8d0482b1427f6d51034165919-585x585.jpg"
                            ICON = "icons8-christmas-star"
                        } else if name == "History" {
                            URL = "https://washington-org.s3.amazonaws.com/s3fs-public/styles/related_listings/public/homepage-hero-museums-for-days-henry-the-elephant-smithsonian-museum-natural-history.jpg?itok=D83SDPKI"
                            ICON = "icons8-mountain"
                        } else {
                            URL = "https://animalsafari.com/MO/wp-content/uploads/2016/03/wild-animal-safar-drive-thru-animal-park-tiger.jpg"
                            ICON = "icons8-cat-footprint"
                        }
                        category.imageURL = URL
                        category.imageIconURL = ICON
                        
                        self.categories.append(category)
                        category.writeToRealm()
                        
                    }
                }
                self.adapter.performUpdates(animated: true, completion: nil)
            }
        })

    }
    
    //MARK: - ListAdapterDataSource
    func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
        return categories
    }
    
    func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        return CategoriesSectionController()
    }
    
    func emptyView(for listAdapter: ListAdapter) -> UIView? {
        let frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
        let view = UIView(frame: frame)
        view.backgroundColor = Colors.colorBG()
        
        let label = UILabel(frame: CGRect(x: 0, y: (frame.height/2)-100, width: frame.width, height: 30))
        label.text = "EMPTY CELL"
        label.textAlignment = .center
        view.addSubview(label)
        
        return view
    }
    
    func didSelect(_ sectionController: ListSingleSectionController, with object: Any) {
    }

}

