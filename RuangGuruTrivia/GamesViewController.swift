//
//  GamesViewController.swift
//  RuangGuruTrivia
//
//  Created by Solihin Chiko on 1/3/18.
//  Copyright © 2018 test. All rights reserved.
//

import UIKit
import IGListKit
import Alamofire
import MBProgressHUD
import RealmSwift
import PopupDialog


class GamesViewController: UIViewController, ListAdapterDataSource, ListSingleSectionControllerDelegate, ButtonSectionDelegate {
    
    func selectedAnswer(_ index: Int, _ correctAnswer:Bool) {
        if correctAnswer {
            rightAnswer = rightAnswer+1
        }
        isSelected = index
        self.adapter.performUpdates(animated: true, completion: nil)
    }
    

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var buttonNext: UIButton!
    @IBOutlet weak var labelQuestionNumber: UILabel!
    @IBOutlet weak var labelRightAnswer: UILabel!
    @IBOutlet weak var topViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewTop: UIView!
    
    
    var ID = Int()
    var data:[Any] = []
    var questionNumber: Int = 0
    var isSelected:Int = -1
    var rightAnswer = 0

    lazy var adapter: ListAdapter = {
        return ListAdapter(updater: ListAdapterUpdater(), viewController: self)
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        adapter.collectionView = collectionView
        adapter.dataSource = self
        
        if UIDevice.isIphoneX {
            topViewConstraint.constant = 84
        } else {
            topViewConstraint.constant = 64
        }

        self.view.backgroundColor = Colors.colorBG()
        navigationItem.hidesBackButton = true
        collectionView.backgroundColor = Colors.colorBG()
        labelQuestionNumber.textColor = Colors.vividPurple()

        let btn1 = UIButton(type: .custom)
        btn1.setImage(UIImage(named: "icons8-delete.png"), for: .normal)
        btn1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn1.addTarget(self, action: #selector(GamesViewController.back), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: btn1)

        self.navigationItem.setLeftBarButtonItems([item1], animated: true)

        if API.isConnectedToInternet() {
            fetchQuestion()
        } else {
            let Category:TriviaCategories = uiRealm.objects(TriviaCategories.self).filter("categoryID = %@", ID).first!
            guard Category.triviaQuestion.count > 0 else {
                // alert
                let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
                hud.mode = .text
                hud.label.text = "Not connect to internet"
                hud.hide(animated: true, afterDelay: 2)
                return
            }
            fetchFromRealm(results: Category.triviaQuestion)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    //MARK: - IBAction
    @IBAction func didTapNextButton(_ sender: Any) {
        isSelected = -1
        questionNumber = questionNumber + 1
        if questionNumber < data.count  {
            updateViewQuestion()
        }
        else {
            // popup
            createPopup()
        }
    }

    //MARK: - Function
    @objc func back() {
        guard self.data.count > 0 else {
            self.navigationController?.popViewController(animated: true)
            return
        }
        createPopupExit()
    }
    
    func updateViewQuestion() {
        labelQuestionNumber.text = "Question no. \(questionNumber + 1)"
        labelRightAnswer.text = "\(rightAnswer) Right Answer"

        if questionNumber == data.count - 1 {
            buttonNext.setTitle("FINISH GAME", for: .normal)
        }
        
        self.adapter.performUpdates(animated: true, completion: nil)
    }
    
    func createPopup() {
        let title = "GAME OVER"
        let message = "Your score is \(rightAnswer)"
        let image = UIImage(named: "pexels-photo-103290")
        
        // Create the dialog
        let popup = PopupDialog(title: title, message: message, image: image)
        
        // Create buttons
        let buttonOne = CancelButton(title: "OK") {
            self.navigationController?.popViewController(animated: true)
        }
        
        // Add buttons to dialog
        // Alternatively, you can use popup.addButton(buttonOne)
        // to add a single button
        popup.addButton(buttonOne)
        
        // Present dialog
        self.present(popup, animated: true, completion: nil)
    }
    
    func createPopupExit() {
        let title = "ARE YOU SURE WANT TO EXIT?"
        let message = "Your score is \(rightAnswer)"
        let image = UIImage(named: "pexels-photo-103290")
        
        // Create the dialog
        let popup = PopupDialog(title: title, message: message, image: image)
        
        // Create buttons
        let buttonCancel = CancelButton(title: "CANCEL") {
            print("CANCEL EXIT")
        }
        
        let buttonOK = CancelButton(title: "OK") {
            self.navigationController?.popViewController(animated: true)
        }
        
        // Add buttons to dialog
        // Alternatively, you can use popup.addButton(buttonOne)
        // to add a single button
        popup.addButtons([buttonCancel, buttonOK])
        popup.buttonAlignment = .horizontal
        
        
        // Present dialog
        self.present(popup, animated: true, completion: nil)
    }
    
    func fetchFromRealm(results:List<TriviaQuestion>) {
        
        for trivia in results {
            // TRIVIAQ
            var buttons:[Button] = []
            let correct:Button = Button(name: trivia.correctAnswer, correct: true, selected: false)
            buttons.append(correct)
            
            for wrongAnswer in trivia.wrongAnswer {
                let wrong:Button = Button(name: wrongAnswer.answer, correct: false, selected: false)
                buttons.append(wrong)
            }
            // shuffle soal
            buttons.shuffle()
            let items = ButtonItem(arrayAnswer: buttons)
            let quest:[Any] = [
                trivia.question,
                items
            ]
            self.data.append(quest)
        }
        self.updateViewQuestion()
    }
    
    func fetchQuestion() {
        let category:TriviaCategories = uiRealm.objects(TriviaCategories.self).filter("categoryID = %@", ID).first!
        // delete yang lama
        try! uiRealm.write {
            uiRealm.delete(category.triviaQuestion)
        }
        
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.label.text = "Loading..."
        hud.show(animated: true)
        
        API.getTriviaQuestion(category: ID, amount: 20, type: "multiple", completion: { (Results) in
            
            hud.hide(animated: true)
            
            let status:[String:Any] = Results.first!
            let results:[String:Any] = Results.last!
            
            if status["status"] as! String == "Failed" {
                
                let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
                hud.mode = .text
                hud.label.text = results["response"] as? String
                hud.hide(animated: true, afterDelay: 2)
            } else {
                for result in results["response"] as! [[String:Any]] {
                    let quiz = TriviaQuestion()
                    quiz.question = result["question"] as! String
                    quiz.correctAnswer = result["correct_answer"] as! String
                    quiz.difficulty = result["difficulty"] as! String
                    quiz.type = result["type"] as! String
                    
                    var buttons:[Button] = []
                    let correct:Button = Button(name: quiz.correctAnswer, correct: true, selected: false)
                    buttons.append(correct)
                    
                    // loop 2
                    let inccorects: [String] = result["incorrect_answers"] as! [String]
                    for inccorect in inccorects {
                        let wrongAnswer = WrongAnswer()
                        wrongAnswer.answer = inccorect
                        quiz.wrongAnswer.append(wrongAnswer)
                        
                        let wrong:Button = Button(name: inccorect, correct: false, selected: false)
                        buttons.append(wrong)
                    }
                    try! uiRealm.write {
                        category.triviaQuestion.append(quiz)
                    }
                    
                    buttons.shuffle()
                    let items = ButtonItem(arrayAnswer: buttons)
                    let quest:[Any] = [
                        quiz.question,
                        items
                    ]
                    self.data.append(quest)
                }
                self.updateViewQuestion()
            }
        })
    }

    // MARK: ListAdapterDataSource
    func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
        var quiz:[ListDiffable] = []

        if data.count > 0 {
            var object:[Any] = data[questionNumber] as! [Any]
            
            viewTop.isHidden = false
            buttonNext.isHidden = false

            if isSelected > -1 {

                let butItem = object.last! as! ButtonItem
                var allButton:[Button] = []
                for (i, button) in butItem.arrayAnswer.enumerated() {
                    if i == isSelected {
                        let selectButton = Button(name: button.name, correct: button.correct, selected: true)
                        allButton.append(selectButton)
                    } else {
                        let unSelectButton = Button(name: button.name, correct: button.correct, selected: false)
                        allButton.append(unSelectButton)
                    }
                }
                
                let newButtonItem = ButtonItem(arrayAnswer: allButton)
                object.removeLast()
                object.append(newButtonItem)
            }
            quiz = object as! [ListDiffable]
        } else {
            viewTop.isHidden = true
            buttonNext.isHidden = true
        }
        return quiz
    }

    func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {

        switch object {

        case is String:
            return QuestionSectionController(questionNumber)
        default:
            if isSelected > -1 {
                let sectionController = ButtonSectionController(true)
                sectionController.delegate = self
                return sectionController
            }
            else {
                let sectionController = ButtonSectionController(false)
                sectionController.delegate = self
                return sectionController
            }
        }
    }

    func emptyView(for listAdapter: ListAdapter) -> UIView? {
        let frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
        let view = UIView(frame: frame)
        view.backgroundColor = Colors.colorBG()

        let label = UILabel(frame: CGRect(x: 0, y: (frame.height/2)-100, width: frame.width, height: 30))
        label.text = "EMPTY CELL"
        label.textAlignment = .center
        view.addSubview(label)

        return view
    }

    func didSelect(_ sectionController: ListSingleSectionController, with object: Any) {
    }

}

extension MutableCollection {
    /// Shuffles the contents of this collection.
    mutating func shuffle() {
        let c = count
        guard c > 1 else { return }

        for (firstUnshuffled, unshuffledCount) in zip(indices, stride(from: c, to: 1, by: -1)) {
            let d: IndexDistance = numericCast(arc4random_uniform(numericCast(unshuffledCount)))
            let i = index(firstUnshuffled, offsetBy: d)
            swapAt(firstUnshuffled, i)
        }
    }
}

extension Sequence {
    /// Returns an array with the contents of this sequence, shuffled.
    func shuffled() -> [Element] {
        var result = Array(self)
        result.shuffle()
        return result
    }
}

extension UIDevice {
    static var isIphoneX: Bool {
        var modelIdentifier = ""
        if isSimulator {
            modelIdentifier = ProcessInfo.processInfo.environment["SIMULATOR_MODEL_IDENTIFIER"] ?? ""
        } else {
            var size = 0
            sysctlbyname("hw.machine", nil, &size, nil, 0)
            var machine = [CChar](repeating: 0, count: size)
            sysctlbyname("hw.machine", &machine, &size, nil, 0)
            modelIdentifier = String(cString: machine)
        }
        
        return modelIdentifier == "iPhone10,3" || modelIdentifier == "iPhone10,6"
    }
    
    static var isSimulator: Bool {
        return TARGET_OS_SIMULATOR != 0
    }
}


