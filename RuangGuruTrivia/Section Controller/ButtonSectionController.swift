//
//  ButtonSectionController.swift
//  RuangGuruTrivia
//
//  Created by Solihin Chiko on 1/3/18.
//  Copyright © 2018 test. All rights reserved.
//

import UIKit
import IGListKit

protocol ButtonSectionDelegate: class {
    func selectedAnswer(_ index: Int, _ correctAnswer:Bool)
}

final class Button:NSObject {
    let name: String
    let correct: Bool
    var selected:Bool

    init(name: String, correct: Bool, selected: Bool) {
        self.name = name
        self.correct = correct
        self.selected = selected
    }
}

final class ButtonItem: NSObject {
    var arrayAnswer: [Button]

    init(arrayAnswer: [Button]) {
        self.arrayAnswer = arrayAnswer
    }
}

extension ButtonItem: ListDiffable {
    func diffIdentifier() -> NSObjectProtocol {
        return self
    }

    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        return self === object ? true : self.isEqual(object)
    }
}

class ButtonSectionController: ListSectionController {

    weak var delegate: ButtonSectionDelegate?

    private var object: ButtonItem?
    var selected = false

    override init() {
        super.init()
        self.minimumInteritemSpacing = 1
        self.minimumLineSpacing = 1
    }

    convenience init(_ selected:Bool) {
        self.init()
        self.selected = selected
    }

    override func numberOfItems() -> Int {
        return (object?.arrayAnswer.count)! //object?.itemCount ?? 0
    }

    override func sizeForItem(at index: Int) -> CGSize {
        return CGSize(width: (collectionContext!.containerSize.width)/2, height:(collectionContext!.containerSize.width)/2 * 0.6)
    }

    override func cellForItem(at index: Int) -> UICollectionViewCell {

        let cell = collectionContext!.dequeueReusableCell(withNibName: ButtonCollectionViewCell.nibName, bundle: Bundle.main, for: self, at: index) as! ButtonCollectionViewCell
        if index == 0 {
            cell.textChoose = "A"
        } else if index == 1 {
            cell.textChoose = "B"
        } else if index == 2 {
            cell.textChoose = "C"
        } else {
            cell.textChoose = "D"
        }
        let button:Button = (object?.arrayAnswer[index])!
        cell.textLabel?.text = button.name.htmlDecoded()
        cell.textLabel2?.text = button.name.htmlDecoded()
        
        
        if selected {
            if button.selected && button.correct {
                cell.textLabel?.textColor = UIColor.white
                cell.color = Colors.aquaMarine()
                cell.lineColor = Colors.darkAquaMarine()
                cell.chooseColor = Colors.aquaMarine()
                
                cell.textCorrect = "Correct Answer"
                cell.textLabelCorrect?.textColor = UIColor.white
                cell.textLabel2?.textColor = UIColor.white
                
                cell.viewCorrect.isHidden = false
                cell.textLabel?.isHidden = true
            }
            else if button.selected && !button.correct {
                cell.textLabel?.textColor = UIColor.white
                cell.color = Colors.coral()
                cell.chooseColor = Colors.coral()
                cell.lineColor = Colors.darkCoral()
                
                cell.textCorrect = "Wrong Answer"
                cell.textLabelCorrect?.textColor = UIColor.white
                cell.textLabel2?.textColor = UIColor.white
                
                cell.viewCorrect.isHidden = false
                cell.textLabel?.isHidden = true
            }
            else if !button.selected && button.correct {
                cell.textLabel?.textColor = UIColor.white
                cell.color = Colors.aquaMarine()
                cell.lineColor = Colors.darkAquaMarine()
                cell.chooseColor = Colors.aquaMarine()
                
                cell.textCorrect = "Correct Answer"
                cell.textLabelCorrect?.textColor = UIColor.white
                cell.textLabel2?.textColor = UIColor.white
                
                cell.viewCorrect.isHidden = false
                cell.textLabel?.isHidden = true
            }
            else {
                cell.textLabel?.textColor = UIColor.black
                cell.color = UIColor.white
                cell.chooseColor = UIColor.black
                cell.lineColor = UIColor.darkGray
                
                cell.textCorrect = ""
                
                cell.viewCorrect.isHidden = true
                cell.textLabel?.isHidden = false
            }
        }
        else {
            cell.textLabel?.textColor = UIColor.black
            cell.color = UIColor.white
            cell.chooseColor = UIColor.black
            cell.lineColor = UIColor.darkGray
            
            cell.textCorrect = ""
            
            cell.viewCorrect.isHidden = true
            cell.textLabel?.isHidden = false
        }
        return cell
    }

    override func didUpdate(to object: Any) {
        self.object = object as? ButtonItem
    }

    override func didSelectItem(at index: Int) {
        guard selected else {
            let button:Button = (object?.arrayAnswer[index])!
            if button.correct {
                delegate?.selectedAnswer(index, true)
            } else {
                delegate?.selectedAnswer(index, false)
            }
            return
        }
        
        
    }

}

