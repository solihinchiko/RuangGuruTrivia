//
//  ButtonCollectionViewCell.swift
//  RuangGuruTrivia
//
//  Created by Solihin Chiko on 1/3/18.
//  Copyright © 2018 test. All rights reserved.
//

import UIKit

class ButtonCollectionViewCell: UICollectionViewCell {
    static let nibName = "ButtonCollectionViewCell"
    
    @IBOutlet weak var textLabel: UILabel?
    @IBOutlet weak var textLabel2: UILabel!
    @IBOutlet weak var textLabelCorrect: UILabel?
    @IBOutlet weak var textLabelChoose: UILabel?
    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var viewLine: UIView!
    @IBOutlet weak var viewChooseLine: UIView!
    @IBOutlet weak var viewCorrect: UIView!
    var isCorrect:Bool = false
    
    var text: String? {
        didSet {
            textLabel?.text = text
        }
    }
    
    var textCorrect: String? {
        didSet {
            textLabelCorrect?.text = textCorrect
        }
    }
    
    var textChoose: String? {
        didSet {
            textLabelChoose?.text = textChoose
        }
    }
    
    var color: UIColor? {
        didSet {
            viewBackground.backgroundColor = color
        }
    }
    
    var lineColor: UIColor? {
        didSet {
            viewLine.backgroundColor = lineColor
            viewChooseLine.backgroundColor = lineColor
        }
    }
    
    var chooseColor: UIColor? {
        didSet {
            textLabelChoose?.textColor = chooseColor
        }
    }
    
}
