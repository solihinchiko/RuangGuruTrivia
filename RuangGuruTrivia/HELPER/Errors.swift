//
//  Errors.swift
//  RuangGuruTrivia
//
//  Created by Solihin Chiko on 1/3/18.
//  Copyright © 2018 test. All rights reserved.
//

import UIKit

struct Errors {
    enum API:Error {
        case internedNotConnected
    }
}

extension Errors.API: CustomStringConvertible {
    var description: String {
        switch self {
        case .internedNotConnected:
            return "Not connect to internet"
        }
    }
}
