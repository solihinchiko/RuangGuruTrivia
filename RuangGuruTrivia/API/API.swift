//
//  API.swift
//  RuangGuruTrivia
//
//  Created by Solihin Chiko on 1/3/18.
//  Copyright © 2018 test. All rights reserved.
//

import Foundation
import Alamofire

class API {
    
    class func isConnectedToInternet() -> Bool {
        return NetworkReachabilityManager()!.isReachable
    }
    
    class func getTriviaCategories(completion: @escaping (_ Result:[[String:Any]]) -> Void) {
        let path = "https://opentdb.com/api_category.php"
        
        Alamofire.request(
            path,
            parameters: nil,
            headers: nil
            )
            .validate(statusCode: 200..<300)
            .responseJSON { response in
                
                
                
                guard response.result.isSuccess else {
//                    print("Error while fetching tags: \(String(describing: response.result.error!.localizedDescription))")
                    let result:[[String:Any]] = [
                        ["status":"Failed"],
                        ["response":response.result.error?.localizedDescription as Any]
                    ]
                    
                    completion(result)
                    return
                }
                
                guard let responseJSON = response.result.value as? [String: Any] else {
//                    print("Invalid tag information received from the service")
                    let result:[[String:Any]] = [
                        ["status":"Failed"],
                        ["response":"Invalid tag information received from the service" as Any]
                    ]
                    completion(result)
                    return
                }
                
                let results:[[String:Any]] = [
                    ["status":"Success"],
                    ["response":responseJSON["trivia_categories"] as? [[String:Any]] as Any]
                ]
                
                completion(results)
        }
        
    }
    
    class func getTriviaQuestion(category: Int, amount: Int, type: String, completion: @escaping (_ Result:[[String:Any]]) -> Void) {
        
        let path = "https://opentdb.com/api.php?amount=\(amount)&category=\(category)&type=\(type)"
        
        Alamofire.request(
            path,
            parameters: nil,
            headers: nil
            )
            .validate(statusCode: 200..<300)
            .responseJSON { response in
                
                guard response.result.isSuccess else {
//                    print("Error while fetching tags: \(String(describing: response.result.error!.localizedDescription))")
                    
                    let result:[[String:Any]] = [
                        ["status":"Failed"],
                        ["response":response.result.error?.localizedDescription as Any]
                    ]
                    
                    completion(result)
                    return
                }
                
                guard let responseJSON = response.result.value as? [String: Any] else {
//                    print("Invalid tag information received from the service")
                    
                    let result:[[String:Any]] = [
                        ["status":"Failed"],
                        ["response":"Invalid tag information received from the service" as Any]
                    ]
                    
                    completion(result)
                    return
                }
                let results:[[String:Any]] = [
                    ["status":"Success"],
                    ["response":responseJSON["results"] as? [[String:Any]] as Any]
                ]
                
                completion(results)
        }
        
    }
    
}

