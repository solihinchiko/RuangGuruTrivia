//
//  QuestionCollectionViewCell.swift
//  RuangGuruTrivia
//
//  Created by Solihin Chiko on 1/3/18.
//  Copyright © 2018 test. All rights reserved.
//

import UIKit

class QuestionCollectionViewCell: UICollectionViewCell {
    static let nibName = "QuestionCollectionViewCell"
    
    @IBOutlet weak var textLabel: UILabel?
    
    var text: String? {
        didSet {
            textLabel?.text = text
        }
    }
    
}
