//
//  QuestionSectionController.swift
//  RuangGuruTrivia
//
//  Created by Solihin Chiko on 1/3/18.
//  Copyright © 2018 test. All rights reserved.
//

import UIKit
import IGListKit

final class QuestionSectionController: ListSectionController {

    private var object: TriviaQuestion!
    private var objectString: String!
    var questionNumber = Int()
    
    override init() {
        super.init()
        inset = UIEdgeInsets(top: 10, left: 5, bottom: 0, right: 5)
        self.minimumInteritemSpacing = 0
        self.minimumLineSpacing = 5
    }
    
    convenience init(_ number:Int) {
        self.init()
        questionNumber = number
    }
    
    override func sizeForItem(at index: Int) -> CGSize {
        return CGSize(width: collectionContext!.containerSize.width - 50, height:80)
    }
    
    override func cellForItem(at index: Int) -> UICollectionViewCell {
        let cell = collectionContext!.dequeueReusableCell(withNibName: QuestionCollectionViewCell.nibName, bundle: Bundle.main, for: self, at: index)
        if let cell = cell as? QuestionCollectionViewCell {
            cell.textLabel?.text =  objectString.htmlDecoded()
        }
        return cell
    }
    
    override func didUpdate(to object: Any) {
        self.objectString = object as? String
    }
    
    override func didSelectItem(at index: Int) {
    }

}

extension String {
    func htmlDecoded()->String {
        
        guard (self != "") else { return self }
        
        var newStr = self
        // from https://en.wikipedia.org/wiki/List_of_XML_and_HTML_character_entity_references
        let entities = [ //a dictionary of HTM/XML entities.
            "&quot;"    : "\"",
            "&amp;"     : "&",
            "&apos;"    : "'",
            "&lt;"      : "<",
            "&gt;"      : ">",
            "&deg;"     : "º",
            "&#039;"    : "'",
            "&Uuml;"    : "Ü",
            "&eacute;"  : "é"
            ]
        
        for (name,value) in entities {
            newStr = newStr.replacingOccurrences(of: name, with: value)
        }
        return newStr
    }
}


