//
//  Colors.swift
//  RuangGuruTrivia
//
//  Created by Solihin Chiko on 1/4/18.
//  Copyright © 2018 test. All rights reserved.
//

import UIKit

class Colors: NSObject {
    class func colorBG() -> UIColor {
        return UIColor(rgb: 0xF4F4F4)
    }
    
    class func vividPurple() -> UIColor {
        return UIColor(rgb: 0x9012FE)
    }
    
    class func aquaMarine() -> UIColor {
        return UIColor(rgb: 0x50E3C2)
    }
    
    class func darkAquaMarine() -> UIColor {
        return UIColor(displayP3Red: 112.0/255, green: 173.0/255, blue: 156.0/255, alpha: 1.0)
    }
    
    class func coral() -> UIColor {
        return UIColor(rgb: 0xFB4040)
    }
    
    class func darkCoral() -> UIColor {
        return UIColor(displayP3Red: 177.0/255, green: 73.0/255, blue: 67.0/255, alpha: 1.0)
    }
}


extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
}
