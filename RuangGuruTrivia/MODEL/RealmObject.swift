//
//  RealmObject.swift
//  RuangGuruTrivia
//
//  Created by Solihin Chiko on 1/3/18.
//  Copyright © 2018 test. All rights reserved.
//

import UIKit
import IGListKit
import RealmSwift

class BaseRealm: Object {
}

extension BaseRealm {
    func writeToRealm() {
        try! uiRealm.write {
            uiRealm.add(self)
        }
    }
    
    func deleteFromRealm() {
        try! uiRealm.write {
            uiRealm.delete(self)
        }
    }
}

class TriviaCategories: BaseRealm {
    @objc dynamic var name = ""
    @objc dynamic var categoryID = 0
    @objc dynamic var imageURL = ""
    @objc dynamic var imageIconURL = ""
    let triviaQuestion = List<TriviaQuestion>()
    
    override static func primaryKey() -> String? {
        return "categoryID"
    }
}

class TriviaQuestion: BaseRealm {
    @objc dynamic var question = ""
    @objc dynamic var type = ""
    @objc dynamic var difficulty = ""
    @objc dynamic var correctAnswer = ""
    let wrongAnswer = List<WrongAnswer>()
}

class WrongAnswer: BaseRealm {
    @objc dynamic var answer = ""
}

extension TriviaCategories: ListDiffable {
    func diffIdentifier() -> NSObjectProtocol {
        return categoryID as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        if self === object { return true }
        return false
    }
}

extension TriviaQuestion: ListDiffable {
    func diffIdentifier() -> NSObjectProtocol {
        return question as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        if self === object { return true }
        return false
    }
}
