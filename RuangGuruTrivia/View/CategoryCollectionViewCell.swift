//
//  CategoryCollectionViewCell.swift
//  RuangGuruTrivia
//
//  Created by Solihin Chiko on 1/3/18.
//  Copyright © 2018 test. All rights reserved.
//

import UIKit
import SDWebImage

class CategoryCollectionViewCell: UICollectionViewCell {
    static let nibName = "CategoryCollectionViewCell"
    
    @IBOutlet weak var textLabel: UILabel?
    @IBOutlet weak var imageView: UIImageView?
    @IBOutlet weak var imageIcon: UIImageView?
    
    var text: String? {
        didSet {
            textLabel?.text = text
        }
    }
    
    var imageURL: String? {
        didSet {
            imageView?.sd_setImage(with: URL(string: imageURL!), placeholderImage: UIImage(named: "placeholder.jpg"))
        }
    }
    
    var imageIconURL: String? {
        didSet {
//            imageIcon?.sd_setImage(with: URL(string: imageIconURL!), placeholderImage: UIImage(named: ""))
            imageIcon?.image = UIImage(named: imageIconURL!)
        }
    }
    
}
